import cPickle as pickle

from base import BaseDataStore
import redis


class RedisDataStore(BaseDataStore):
    def __init__(self, redis_conf, get_cache_size=0, execute_cache_size=0):
        super(RedisDataStore, self).__init__(get_cache_size, execute_cache_size)
        self.redis = redis.StrictRedis(**redis_conf)

    def _get(self, series_name_or_op):
        return pickle.loads(self.redis.get(self._get_key(series_name_or_op)))

    def _set(self, series_name_or_op, series):
        key = self._get_key(series_name_or_op)
        if series:
            self.redis.sadd('ops', key)
        else:
            self.redis.srem('ops', key)
        return self.redis.set(key, pickle.dumps(series))

    def iterkeys(self):
        for key in self.redis.sscan_iter('ops'):
            yield key

    def iteritems(self):
        for key in self.iterkeys:
            yield self.get(key)

    def clean(self):
        return self.redis.flushall()

    def remove(self, name):
        key = self._get_key(name)
        self.redis.srem('ops', key)
        self.redis.delete(key)
        return key

    def __contains__(self, name):
        return self.redis.sismember('ops', self._get_key(name))

    def save(self, series_name_or_op, values):
        self._set(series_name_or_op, values)
